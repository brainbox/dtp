FROM centos:centos6

MAINTAINER Gary Straughan <gary.straughan@brainboxweb.co.uk>

#RUN yum -y update

RUN yum -y install httpd gcc make git mysql-server mysql mysql-client python-setuptools

RUN rpm -Uvh http://ftp.iij.ad.jp/pub/linux/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
RUN rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
RUN yum -y install --enablerepo=remi --enablerepo=remi-php56 php php-opcache php-devel php-mbstring php-mcrypt php-mysqlnd php-pecl-xdebug php-pecl-xhprof pcre-devel php-gd php-gmp php-intl openssl-devel


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD vm/vhost.conf /etc/httpd/conf.d/vhost.conf


EXPOSE 80

WORKDIR /var/www/dtp

CMD tail -F /var/log/httpd/* & exec httpd -D FOREGROUND
