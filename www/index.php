<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

use Symfony\Component\HttpFoundation\Request;

date_default_timezone_set('Europe/London');

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;
use JasonGrimes\Paginator;


$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../templates'
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new ThisPageCannotBeFound\Silex\Provider\MarkdownServiceProvider());


$content = file_get_contents(__DIR__.'/../data/content.yml');
$content = Yaml::parse($content);

$posts = file_get_contents(__DIR__.'/data/posts.json');

try {
    $posts = json_decode($posts, true);
    // var_dump($posts);

} catch (Exception $e) {
    print_r($e->getMessage());
}
krsort($posts);


//------- Home
$app['pageHome'] = $content['home'];

$app['featuredposts'] = array_slice($posts, 0, 3, true);
$app['cheatsheets'] = $content['cheatsheets'];

//var_dump($app['cheatsheets']);

$app->get('/', function (Silex\Application $app)  { 
    return $app['twig']->render( 
        'index.html.twig',
        array(
            'page' => $app['pageHome'],
            'posts' => $app['featuredposts'],
            'cheatsheets' => getCheatsheetsFromContent($app)
        )
    );
});


/*
$app->get('/courses/scrumban-blueprint', function (Silex\Application $app)  { 
    return $app->redirect('https://community.developmentthatpays.com/plans/47161?bundle_token=3c000d700cf200362d4e274266861ba0&utm_source=manual');
});
*/


//------- Blog
$app['pageBlog'] = $content['blog'];
$app['posts'] = $posts;

$app->get('/blog/{pageNumber}', function (Silex\Application $app, $pageNumber)  {

    $page = $app['pageBlog'];
    if ($pageNumber > 1){
        $page['noindex'] = true;
    }

    $totalItems = count($app['posts']);
    $itemsPerPage = 9;
    $currentPage = $pageNumber;
    $urlPattern = '/blog/(:num)';

    $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

    $posts = array_slice($app['posts'], 9 * ($pageNumber-1), 9);

    return $app['twig']->render(
        'blog.html.twig',
        array(
            'page' => $page,
            'posts' =>  $posts,
            'paginator' => $paginator
        )
    );
})->value('pageNumber', 1); //Note this magic


//------- PrivacyPolicy
$app['pagePrivacy'] = $content['privacy-policy'];

$app->get('/privacy-policy', function (Silex\Application $app)  {
    return $app['twig']->render(
        'privacy.html.twig',
        array(
            'page' => $app['pagePrivacy'],
        )
    );
});



//------- Posts
$app['posts'] = $posts;
$app->get('/posts/{slug}', function (Silex\Application $app, $slug)  {

    //Need to split the slug
    $parts = explode('-', $slug);
    $id = $parts[0];

    if (!isset($app['posts'][$id])) {
        $app->abort(404, "Post $id does not exist.");
    }

    $post = $app['posts'][$id];
    return $app['twig']->render(
        'post.html.twig',
        array(
            'slug' => $slug,
            'post' => $post,
            'page' => array(
                'title' => $post['title'],
                'metatitle' => '[VIDEO] ' . $post['title'],
                'metadescription' => $post['description'] . ' [VIDEO]'
            )
        )
    );
})
//    ->assert('id', '\s+') // specify that the ID should be an integer
    ->bind('post'); // name the route so it can be referred to later in the section


//@todo - remove?
//------- Transcripts
//$app['posts'] = $posts;
$app->get('/transcripts/{slug}', function (Silex\Application $app, $slug)  {

    //Need to split the slug
    $parts = explode('-', $slug);
    $id = $parts[0];

    if (!isset($app['posts'][$id])) {
        $app->abort(404, "Post $id does not exist.");
    }

    $post = $app['posts'][$id];
    return $app['twig']->render(
        'transcript.html.twig',
        array(
            'slug' => $slug,
            'post' => $post,
            'page' => array(
                'title' => $post['title'],
                'metatitle' => $post['title'],
                'metadescription' => $post['description']
            )
        )
    );
})
//    ->assert('id', '\s+') // specify that the ID should be an integer
  ->bind('transcript'); // name the route so it can be referred to later in the section



$app->get('/why', function (Silex\Application $app)  {
    return $app['twig']->render( 
        'page.html.twig',
        array(
            'page' => $app['pageWhy'], 
        )
    );
});


//------- How
//$app['pageHow'] = $content['how'];
//$app->get('/how', function (Silex\Application $app)  {
//    return $app['twig']->render(
//        'page.html.twig',
//        array(
//            'page' => $app['pageHow'],
//        )
//    );
//});


//------- About
$app['pageAbout'] = $content['about'];
$app->get('/about', function (Silex\Application $app)  { 
    return $app['twig']->render( 
        'about.html.twig',
        array(
            'page' => $app['pageAbout'], 
        )
    );
});


//------- Contact
$app['pageContact'] = $content['contact'];
$app->get('/contact', function (Silex\Application $app)  { 
    return $app['twig']->render( 
        'contact.html.twig',
        array(
            'page' => $app['pageContact'], 
        )
    );
});




//------- Scrumban
$app['pageContact'] = $content['contact'];
$app->get('/scrumban', function (Silex\Application $app)  { 
    return $app['twig']->render( 
        'orphans/scrumban.html.twig',
        array(
            'page' => array(
                // 'title' => "Scrumban",
                'metatitle' => "Scrumban",
                'metadescription' => "Scrumban",
            )
        )


    );
});









//------ Landing Page aka Ophan page ----// (Metadata may be tricky)
$app['pageLanding'] = $content['landing']; // This is generic. Need to override in most cases
$app->get('/-/{slug}', function (Silex\Application $app, $slug)  {

    $template = 'landing/' . $slug . '.html.twig';

    $data = $app['pageLanding'];

    if ($slug == 'estimating-minicourse') {
        $data['title'] = 'FREE MINI-COURSE: Agile Estimating and Planning';
        $data['metatitle'] = 'FREE MINI-COURSE: Agile Estimating and Planning';
        $data['metadescription'] = 'Grab this FREE training. But be quick: it goes away soon!';
    }

    // //Counting page views!
    // $counted = [
    //     'scrumkanban-landing',
    //     'scrumkanban-thanks',
    //     'scrumkanban-landing2',
    //     'scrumkanban-thanks2'
    // ];
    // if (in_array($slug, $counted)) {
    //     $fullpath = "../data/$slug.txt";
    //     $fp = fopen($fullpath, a);
    //     $fields = [
    //         'REQUEST_TIME',
    //         'REQUEST_METHOD',
    //         'HTTP_HOST',
    //         'QUERY_STRING',
    //         'HTTP_REFERER',
    //         'HTTP_USER_AGENT',
    //         'REMOTE_HOST',
    //         'REMOTE_ADDR'
    //     ];

    //     $request = $_SERVER;
    //     $requestArray = [];
    //     foreach ($fields as $field) {
    //         if (isset($request)) {
    //             $requestArray[] = $request[$field];
    //         } else {
    //             $requestArray[] = '';
    //         }
    //     }
    //     fputcsv($fp, $requestArray);
    //     fclose($fp);
    // }

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});



//$app['cheatsheets'] = $content['cheatsheets'];
//$app['cheatsheets'] = getCheatsheetsFromContent($app);
$app['cheatsheets'] = $content['cheatsheets'];


$app->get('/cheatsheets', function (Silex\Application $app) {

    $template = "cheatsheets.html.twig";
    $page = $app['cheatsheets'];
    $cheatsheets = getCheatsheetsFromContent($app);

//    $cheatsheets = $data['items'];



    return $app['twig']->render(
        $template,
        array(
            'page' => $page,
            'cheatsheets' => $cheatsheets
        )
    );
});

//----- CheatSheet
$app->get('/cheatsheets/{slug}', function (Silex\Application $app, $slug) {

    if ($slug == "test") {
        $template = "cheatsheet-test.html.twig";

        return $app['twig']->render(
            $template,

            array(
                'page' => array(
                    'title' => 'Scrum vs Kanban Crash Course',
                    'metatitle' => 'Scrum vs Kanban Crash Course',
                    'metadescription' => 'Scrum vs Kanban Crash Course',
                    'version' => 'offer'
                )
            )
        );
    }

    $template = "cheatsheet.html.twig";
    


    $data = $app['cheatsheets'];
    $cheatsheets = getCheatsheetsFromContent($app);

    foreach ($cheatsheets as $k => $cheatsheet) {

        if ($k == $slug) {
            $selected = $cheatsheet;
            break;
        }
    }


    if (!$selected) {
        $app->abort(404, "Course '$slug'' does not exist.");
    }

    $data = array_merge($data, $selected);
    $data['id'] = $slug;
    unset($data['items']);
    

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});



//----- AIO Offer 2021-01
$app->get('/secret/aio-2021', function (Silex\Application $app) {

    $template = 'offers/aio-offer-2021-01.html.twig';
    
    return $app['twig']->render(
        $template,
        array(
            'page' => array(
                'title' => 'Agile Inside Out',
                'metatitle' => 'Agile Inside Out',
                'metadescription' => 'Agile Inside Out',
            )
        )
    );
});



//----- SVK Offer 2021-01
$app->get('/secret/svk-2021', function (Silex\Application $app) {

    $template = 'offers/svk-offer-2021-01.html.twig';
    
    return $app['twig']->render(
        $template,
        array(
            'page' => array(
                'title' => 'Scrum vs Kanban Crash Course',
                'metatitle' => 'Scrum vs Kanban Crash Course',
                'metadescription' => 'Scrum vs Kanban Crash Course',
            )
        )
    );
});


//----- Temporary - SVK Offers
$app->get('/secret/svk-offer', function (Silex\Application $app) {

    $template = 'offers/svk-offer-2021-01.html.twig';

    return $app['twig']->render(
        $template,
        array(
            'page' => array(
                'title' => 'Scrum vs Kanban Crash Course',
                'metatitle' => 'Scrum vs Kanban Crash Course',
                'metadescription' => 'Scrum vs Kanban Crash Course',
                'version' => 'offer'
            )
        )
    );
});



//----- Secret - CheatSheets LIST
$app->get('/secret/cheatsheets', function (Silex\Application $app) {

    $template = "orphans/cheatsheets/cheatsheets.html.twig";
    $data = $app['cheatsheets'];
    $cheatsheets = $data['items'];


//    foreach ($data['items'] as $item){
        foreach ($cheatsheets as $k => $cheatsheet) {
            $cheatsheet['id'] = $k;
            $data['cheatsheets'][] = $cheatsheet;
        }
//    }
    unset($data['items']);

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
            'cheatsheets' => $cheatsheets
        )
    );
});

//----- Secret - CheatSheets
$app->get('/secret/cheatsheets/{slug}', function (Silex\Application $app, $slug) {

    if (!isset($app['cheatsheets']['items'][$slug])) {
        $app->abort(404, "Cheatsheet $slug does not exist.");
    }

    $template = "orphans/cheatsheets/cheatsheet-delivery.html.twig";
    $data = $app['cheatsheets']['items'][$slug];

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});


//----- Courses ---------------------------------------------------------------

$app['courses'] = $content['courses'];
$app->get('/courses', function (Silex\Application $app) {

    $template = "courses.html.twig";

    $data = $app['courses'];

    $courses = $data['items'];



    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
            'courses' => $courses
        )
    );
});


//----- Course ---------------------------------------------------------------

$app->get('/courses/{slug}', function (Silex\Application $app, $slug) {

    $template = "course.html.twig";
    $data = $app['courses'];

    $selected = null;
    foreach ($data['items'] as $k => $course) {
        if ($k == $slug) {
            $selected = $course;
            break;
        }
    }

    if (!$selected) {
        $app->abort(404, "Cheatsheet $slug does not exist.");
    }

    $data = array_merge($data, $selected);
    $data['id'] = $slug;
    unset($data['items']);


    //Override the template
    if (isset($data["template"])) {
        $template = $data['template'];
    }

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});


//----- Course ---------------------------------------------------------------
$app['course-delivery'] = $content['course-delivery'];
$app->get('/course-delivery/scrum-vs-kanban/{slug}', function (Silex\Application $app, $slug) {

    $template = "orphans/courses/course-delivery.html.twig";
    $data = $app['course-delivery'];

    $selected = null;
    foreach ($data['items'] as $k => $course) {
        if ($k == $slug) {
            $selected = $course;
            break;
        }
    }

    if (!$selected) {
        $app->abort(404, "Cheatsheet $slug does not exist.");
    }

    $data = array_merge($data, $selected);
    $data['id'] = $slug;
    unset($data['items']);

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});



// //----- AGILE INSIDE OUT ---------------------------------------------------------------
$app['aio'] = $content['agile-inside-out'];
$app->get('/agile-inside-out', AIOHandler);
$app->get('/agile-inside-out/{slug}', AIOHandler);
function AIOHandler(Silex\Application $app, Request $request, $slug = null) {

    $data = $app['aio'];
//     $closingDate = strtotime($data['closingDate']);

//     // Get time
//     $now = time();
//     $nowString =  $request->get('now');
//     if (!empty($nowString)) {
//         $now = strtotime($nowString);
//         $data['nowString'] = $nowString; // Only in this case!
//     }

//     //Thanks
//     if ($slug == 'thanks') {
//         $data['email_address'] =  $request->get('drip_email');
//         $template  = 'orphans/courses/aio-thanks.html.twig';
//         return $app['twig']->render(
//             $template,
//             array(
//                 'page' => $data,
//             )
//         );
//     }

//     //Waitlist Thanks
//     if ($slug == 'waitlist-thanks') {
//         $data['email_address'] =  $request->get('drip_email');
//         $template  = 'orphans/courses/aio-waitlist-thanks.html.twig';
//         return $app['twig']->render(
//             $template,
//             array(
//                 'page' => $data,
//             )
//         );
//     }

    //Waitlist
    if ( $slug == '' ||  $slug == 'waitlist' ) { 
        $template  = 'orphans/courses/aio-waitlist.html.twig';
        $data = array_merge($data, $data['waitlist']);
        return $app['twig']->render(
            $template,
            array(
                'page' => $data,
            )
        );
    }

//     //Waitlist II
//     if ($now > $closingDate) {  /// NB - !!!THIS MATCHES A LOT!!!!
//        return $app->redirect('/agile-inside-out/waitlist');
//     }

//     // Landing page/registration page
//     if ($slug === null) {

//         //When CLOSED
//         $template  = 'orphans/courses/aio-waitlist.html.twig';
//         //when OPEN
//         // $template  = 'orphans/courses/aio-register.html.twig';

//         // $data = array_merge($data, $data['register']);
//         return $app['twig']->render(
//             $template,
//             array(
//                 'page' => $data,
//             )
//         );
//     }


//     // Main 4-stage video pages

//     $timeLeft = $closingDate - $now;
//     $data['timeLeftString'] = seconds2human($timeLeft);
//     $template = "orphans/courses/aio-promo.html.twig";
//     // Figure out which are valid
//     foreach ($data['videos'] as $k => $video) {
//         $vidDate = strtotime($video['date']);
//         $data['videos'][$k]['isActive'] = false;
//         if ( $now > $vidDate) {
//             $data['videos'][$k]['isActive'] = true;
//         }
//     }

//     $campaignStatus = "active";
//     $first = reset($data['videos']);
//     if ($first['isActive'] === false){
//         $campaignStatus = "pre";
//     }

//     $data['campaignStatus'] = $campaignStatus;

//     $selected = null;
//     foreach ($data['videos'] as $k => $video) {
//         if ($k == $slug) {
//             $selected = $k;
//             break;
//         }
//     }
//     if (!$selected) { 
//         $app->abort(404, "AIO Video Lesson not found."); // @TODO - MUCH better 404 page.
//     }

//     // Or if it is too soon
//     if ($data['videos'][$k]['isActive'] == false ) { 
//         // Think I'm going to let this through. (Not properly handled on front end.)
//     }

//     $data['selected'] = $selected;
//     foreach ($data['videos'] as $k => $video) {
//         $data['videos'][$k]['isSelected'] = false;
//     }
//     $data['videos'][$selected]['isSelected'] = true;

//     // var_dump($data);

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
}

function seconds2human($ss){
    $s = $ss%60;
    $m = floor(($ss%3600)/60);
    $h = floor(($ss%86400)/3600);
    $d = floor(($ss%2592000)/86400);

    if ($d > 1) {
        return "$d days";
    }
    $h += $d*24;
    if ($h > 1) {
        return "$h hours";
    }
    $m += $h*60;
    if ($m > 1) {
        return "$m minutes";
    }
    
    return "a few seconds";
}


//-------  Sitemap   -----------------------------------------------------------

$app->get('/sitemap.xml', function (Silex\Application $app)  {

    $pages = [];
    $pages[] = '';
    $pages[] = 'about';
    $pages[] = 'contact';
    $pages[] = 'privacy-policy';
    $pages[] = 'scrumban';



//    $pages[] = 'cheatsheets';  //noindex
//    $pages[] = 'courses'; //noindex


    $data = $app['cheatsheets'];
    $cheatsheets = $data['items'];
    //Fix the item keys
    $cheatsheetsNew = [];
    foreach ($cheatsheets as $k => $cheatsheet) {
        //Get rid of the first bit of the key
        $key = explode('-', $k) ;
        array_shift($key);
        $key= implode('-', $key);
        $cheatsheetsNew[$key] = $cheatsheet;
    }


    $data = $app['courses'];
    $courses = $data['items'];
    //Fix the item keys
//    $cheatsheetsNew = [];
//    foreach ($cheatsheets as $k => $cheatsheet) {
//        //Get rid of the first bit of the key
//        $key = explode('-', $k) ;
//        array_shift($key);
//        $key= implode('-', $key);
//        $cheatsheetsNew[$key] = $cheatsheet;
//    }



    return new Response(
        $app['twig']->render(
            'sitemap.xml.twig',
            array(
                'posts' => $app['posts'],
                'pages' => $pages,
                'cheatsheets' => $cheatsheetsNew,
                'courses'=> $courses
            )
        ),
        200,
        ['Content-Type' => 'application/xml']
    );
});

// https://www.developmentthatpays.com/files/DevelopmentThatPays-Scrumban-CheatSheet-3_0.pdf
// sftp://garystra@ftp.garystraughan.com:378/DevelopmentThatPays-Scrumban-Cheatsheet-3_0.pdf
//----- Delivery Page
$app['pageDelivery'] = $content['delivery'];
$app->get('/d/{slug}', function (Silex\Application $app, $slug)  {

    $template = "delivery/$slug.html.twig";

    $data = $app['pageDelivery'];

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});


//----- Delivery Page
$app['surveyResults'] = $content['survey_results'];
$app->get('/HYTF$56FCX_survey_results', function (Silex\Application $app)  {

    $template = "orphans/survey/survey_results.html.twig";

    $data = $app['surveyResults'];

    return $app['twig']->render(
        $template,
        array(
            'page' => $data,
        )
    );
});


// 404 - Page not found
$app['page404'] = $content['404'];
$app->error(function (\Exception $e, $code) use ($app) {
    switch ($code) {
        case 404:
            $template = '404.html.twig';

            return $app['twig']->render(
                $template,
                array(
                    'page' => $app['page404'],
                )
            );
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }
});

$app->run();


function getCheatsheetsFromContent($content){
    $cheatsheets = [];
    foreach ($content["cheatsheets"]["items"] as $k => $cheatsheet) {

        //Get rid of the first bit of the key
        $key = explode('-', $k) ;
        array_shift($key);
        $key= implode('-', $key);
        $cheatsheets[$key] = $cheatsheet;
    }
    return $cheatsheets;
}

//https://www.youtube.com/watch?v=91B_YEG4ooA&list=PLngnoZX8cAn8mVehgSYr2TzFnSdeOMEX4&index=1
//https://www.youtube.com/watch?v=W9c1B1PrJ2w&index=3&list=PLngnoZX8cAn8mVehgSYr2TzFnSdeOMEX4&t=191s
//https://www.youtube.com/watch?v=W9c1B1PrJ2w


