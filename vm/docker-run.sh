#!/bin/sh


printf "\n\nRunning the container"
CONTAINER_ID=`docker run  -d  -v $(pwd):/var/www/dtp -p 8084:80  --add-host dtp.local:127.0.0.1  --name dtp  brainbox/dtp `
CONTAINER_ID=`echo "$CONTAINER_ID"  | cut -c1-11`
printf "\nCreated container $CONTAINER_ID\n"


#printf "\n\nRunning startup scripts\n"
#docker exec $CONTAINER_ID scripts/start.sh


printf "\n\nContainer running on localhost:8084\n"


printf '\n------------------------------------\n\n\n'
printf 'Useful commands:\n\n'
printf "docker exec -it $CONTAINER_ID /bin/bash\n"
printf "docker exec -it $CONTAINER_ID composer install\n"
printf "docker exec -it $CONTAINER_ID bin/phpunit\n"
printf "docker exec -it $CONTAINER_ID bin/behat\n"
printf "\n\n------------------------------------\n\n"
