#!/bin/sh


#boot2docker init

#
#printf "\n\nCopying your id_rsa key\n"
#cp ~/.ssh/id_rsa id_rsa
#
printf "\n\nStopping running container (if any)\n"
docker stop $(docker ps | grep brainbox/dtp |  awk '{print $1}')

#
printf "\n\nBuilding brainbox/dtp\n"

docker build -t brainbox/dtp .



printf "\n\Built brainbox/dtp"
